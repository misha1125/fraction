package com.example.victor.fractionapp;

/**
 * Created by victor on 27.10.16.
 */
public class Fraction {

    private int nominator;
    private int denominator;

    public Fraction () {
        nominator = 0;
        denominator = 1;
    }

    public Fraction (int n, int d) {
        nominator = n;
        denominator = d;
    }

    public Fraction (Fraction f) {
        nominator = f.nominator;
        denominator = f.denominator;
    }


    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }


    public Fraction add (Fraction f) {
        Fraction rez = new Fraction();
        rez.denominator = denominator * f.denominator;
        rez.nominator = nominator * f.denominator + f.nominator * denominator;
        rez.Refuze();
        return rez;
    }

    public Fraction sub(Fraction f2) {
        return add(new Fraction(-f2.nominator,f2.denominator));
    }

    public Fraction mult(Fraction f2) {
        Fraction rez =  new Fraction(nominator * f2.nominator,denominator * f2.denominator);
        rez.Refuze();
        return rez;
    }

    public Fraction div(Fraction f2) {
        return  mult(new Fraction(f2.denominator,f2.nominator));
    }

    void Refuze(){
        int nok;
        int a = nominator;
        int b = denominator;

        while (a!=0 && b!=0){
            if(a > b)
                a = a % b;
            else
                b = b % a;
        }
        nok = a+b;
        nominator/=nok;
        denominator/=nok;
    }
}
